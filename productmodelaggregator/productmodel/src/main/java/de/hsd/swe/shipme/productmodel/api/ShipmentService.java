package de.hsd.swe.shipme.productmodel.api;

public enum ShipmentService {
	COD, 		// cash on delivery (Nachnahme) 
	IDENT, 		// identity check 
	LOCATION,	// preferred location
	NEIGHBOUR,	// preferred neighbour
	NOTIFY		// custom added
}

