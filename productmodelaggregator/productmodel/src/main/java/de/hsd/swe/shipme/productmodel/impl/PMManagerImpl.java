package de.hsd.swe.shipme.productmodel.impl;
import java.util.Set;

import de.hsd.swe.shipme.productmodel.api.PMManager;
import de.hsd.swe.shipme.productmodel.api.ShipmentService;

public class PMManagerImpl implements PMManager {
	
	/**
	 * @see de.hsd.swe.shipme.business.productmodel.impl.PMManager#isValidCombination(java.util.Set)
	 */
	@Override
	public boolean isValidCombination(Set<ShipmentService> services) {
		boolean result;

		if ( services == null) {
			result = true;
		}
		else if (services.size() <= 1) { 
			result = true;
		} else if (services.contains(ShipmentService.LOCATION) || services.contains(ShipmentService.NEIGHBOUR)) {
			result = false;
		} else {
			result = true;
		}
		return result;
	}
	
	/**
	 * @see de.hsd.swe.shipme.business.productmodel.api.PMManager#checkAge
	 */
	@Override
	public boolean checkAge(int actualAge, int minAge) {
		boolean result = false;
		if (actualAge < 0 || minAge < 16 || minAge > 18) {
			throw new IllegalArgumentException("Parameters not within bounds.");
		} else {
			if (actualAge >= minAge) {
				result = true;
			}
		}
		return result;
	}
}

