package de.hsd.swe.shipme.productmodel.api;

import java.util.Set;

public interface PMManager {
	/**
	 * Checks, if a service combination is valid, i.e., does not contain 
	 * two services that are mutually exclusive. In short, valid combinations
	 * do not contain NEIGHBOUR or LOCATION among other services. 
	 * @param 	services A set of services 
	 * @see 	de.hsd.swe.shipme.productmodel.api.ShipmentService
	 * @return 	true iff <code>services</code> does not 
	 * 			contain a pair services that exclude each other
	 */
	boolean isValidCombination(Set<ShipmentService> services);
	
	/**
	 * Checks, if an actual age matches a minimal age
	 * @param actualAge	a positive actual age <= 100
	 * @param minAge	minimal age in the interval [16,18]
	 * @return	true iff <code>acutalAge >= minAge</code>
	 */
	boolean checkAge(int actualAge, int minAge);
}