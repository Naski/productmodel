package de.hsd.swe.shipme.productmodel.impl;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

import de.hsd.swe.shipme.productmodel.api.ShipmentService;

public class PMManagerImplTest {

	@Test
	void isValidCombination_emptySet_true() {
		assertTrue(new PMManagerImpl().isValidCombination(Collections.EMPTY_SET));
	}
	
	@Test
	//@Disabled
	void isValidCombination_singleService_true() {
		Set<ShipmentService> singleService = new HashSet<>();
		singleService.add(ShipmentService.LOCATION);
		assertTrue(new PMManagerImpl().isValidCombination(singleService));
	}
	
	@Test
	//@Disabled
	void isValidCombination_noService_true() {
		final boolean result = new PMManagerImpl().isValidCombination(null);
		assertTrue(result);
	}
	
	
	
	@Test
	void checkAge_Anweisungsueberdeckung() {
		assertTrue(new PMManagerImpl().checkAge(20, 18));
		assertThrows(IllegalArgumentException.class, () -> {
			new PMManagerImpl().checkAge(-3, 44);
		});
	}
	
	@Test
	void checkAge_Zweigueberdeckung() {
		assertTrue(new PMManagerImpl().checkAge(20, 18));
		assertFalse(new PMManagerImpl().checkAge(12, 18));
		assertThrows(IllegalArgumentException.class, () -> {
			new PMManagerImpl().checkAge(-3, 44);
		});
	}	
	
	@Test
	void checkAge_EinfacheBedingungsueberdeckung() {
		assertTrue(new PMManagerImpl().checkAge(20, 18));
		assertFalse(new PMManagerImpl().checkAge(14, 18));
		assertThrows(IllegalArgumentException.class, () -> {
			new PMManagerImpl().checkAge(5, 19);
		});
		assertThrows(IllegalArgumentException.class, () -> {
			new PMManagerImpl().checkAge(5, 15);
		});
		assertThrows(IllegalArgumentException.class, () -> {
			new PMManagerImpl().checkAge(-3, 17);
		});
	}
}
