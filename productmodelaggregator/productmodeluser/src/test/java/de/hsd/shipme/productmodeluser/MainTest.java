package de.hsd.shipme.productmodeluser;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

public class MainTest {

    private final ByteArrayOutputStream outContent = new ByteArrayOutputStream();
    private final PrintStream originalOut = System.out;

    @BeforeEach
    public void setUpStreams() {
        System.setOut(new PrintStream(outContent));
    }

    @AfterEach
        public void restoreStreams() {
        System.setOut(originalOut);
    }

    @Test
    void isValidCombination_COD_NEIGHBOUR_false() {
        Main.main(new String[] {"COD,NEIGHBOUR"});
        assert(outContent.toString().trim().endsWith("false"));
    }
}

