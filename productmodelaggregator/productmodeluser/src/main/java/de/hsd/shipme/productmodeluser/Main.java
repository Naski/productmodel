package de.hsd.shipme.productmodeluser;

import de.hsd.swe.shipme.productmodel.api.PMManager;
import de.hsd.swe.shipme.productmodel.api.ShipmentService;
import de.hsd.swe.shipme.productmodel.impl.PMManagerImpl;

import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Main {

    public static void main(String[] args) {
        PMManager pmManager = new PMManagerImpl();
        //Set<ShipmentService> testSet = Stream.of(ShipmentService.COD, ShipmentService.NEIGHBOUR, ShipmentService.NOTIFY).collect(Collectors.toCollection(HashSet::new));
       Set<ShipmentService> testSet = Stream.of(args[0].split(",")).map(s -> ShipmentService.valueOf(s)).collect(Collectors.toCollection(HashSet::new)); 
	System.out.println("Ist " + testSet + " eine gültige Service-Kombination?: " + pmManager.isValidCombination(testSet));
    }

}
